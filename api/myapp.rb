require 'rack/conneg'
require 'rack/replace_http_accept'
require 'rack/rest_api_versioning'
require 'sinatra'
require 'active_record'

ActiveRecord::Base.establish_connection(
  :adapter => 'sqlite3',
  :database =>  'db/test.sqlite3.db',
  :pool => 100
)


   # Set default API version
   use Rack::RestApiVersioning, :default_version => '1'

   # Create custom vendor mime type mappings
   use Rack::ReplaceHttpAccept, /application\/vnd\.mycompany\.myapp-v[0-9]+\+json/ => 'application/json',
   /application\/vnd\.mycompany\.myapp-v[0-9]+\+xml/  => 'application/xml'

   # Initalise Conneg
   use(Rack::Conneg) { |conneg|
      conneg.set :accept_all_extensions, false
      conneg.provide([:json, :xml])
   }
  class Message < ActiveRecord::Base
  end

   get '/messages' do
      version = env['api_version']
      respond get_messages(version)
   end
  post '/messages' do
    version = env['api_version']
    set_message(version)
  end

  def get_messages version
    @data = Message.all
  end

  def set_message version
    @message = Message.new(:text => params[:text])
    @message.save
  end

   def respond data
      respond_to do |wants|
         wants.json  {
            # Set Content-Type header accordingly
            content_type "application/vnd.mycompany.myapp-v1+json"
            data.to_json
         }
         wants.xml   {
            content_type "application/vnd.mycompany.myapp-v1+xml"
            data.to_xml
         }
         wants.other {
            content_type 'text/plain'
            error 406, "Not Acceptable"
         }
      end
   end

   before do
      if negotiated?
         # Important: resource cacheable based on Content-Type
         headers "Vary" => "Content-Type"
      end
   end
