require('messages/vendor/jquery-1.7.1');
require('messages/vendor/ember-0.9.5');
require('messages/templates/main_view');


Messages = Ember.Application.create({
    // When everything is loaded.
    ready: function() {

        // Start polling
        setInterval(function() {
            Messages.messagesController.refresh();
        }, 2000);

        // Call the superclass's `ready` method.
        this._super();
    }
});

// Model
Messages.Message = Ember.Object.extend({
    id: null,
    text: null
});

// Controller
Messages.messagesController = Ember.ArrayController.create({
    content: [],

    //This var is used to control the duplicates
    _idCache: {},


    // This function adds a new message on the list. Checks if the message already exists
    addMessage: function(message) {
        // The `id` from Twitter's JSON
        var id = message.get("id");

        // If we don't already have an object with this id, add it.
        if (typeof this._idCache[id] === "undefined") {
            this.pushObject(message);
            this._idCache[id] = message.id;
        }
    },

    // Create a message sending a post to the API and we refresh to update the screen
    createMessage: function(text) {
                       var message = Messages.Message.create({ text: text });
                       json = {text:message.text}
                       $.ajax({
                           type: 'POST',
                           url: "http://127.0.0.1:4567/messages",
                           data: json,
                           dataType: 'json'
                       });
                       this.refresh()
                   },

    // How many messages we have
    counter: function() {
                 return this.get('length');
             }.property('@each.text'),

    isEmpty: function() {
                 return this.get('length') === 0;
             }.property('length'),

    // This method is called by the poller we configured previously. We get all the messages and add each message to the screen
    refresh: function() {
                 // Poll 
                 var self = this;
                 var url = "http://127.0.0.1:4567/messages";
                 $.getJSON(url, function(data) {
                     // Make a model for each result and add it to the collection.

                     for (var i = 0; i < data.length; i++) {
                         self.addMessage(Messages.Message.create(data[i].message));
                     }
                 });
             }.observes("query")


});

// Used to add a new message
Messages.CreateMessageView = Ember.TextField.extend({
    insertNewline: function() {
                       var value = this.get('value');

                       if (value) {
                           Messages.messagesController.createMessage(value);
                           this.set('value', '');
                       }
                   }
});

//Creating the view
Messages.MainView = Ember.View.extend({
    templateName: 'main_view'
});
